import { FormEvent, useContext, useState } from 'react'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

import logoImg from '../../../public/logo.svg'
import { Button } from '../../components/ui/Button'
import { Input } from '../../components/ui/Input'

import styles from '../../styles/home.module.scss'
import { toast } from 'react-toastify'
import { AuthContext } from '../../contexts/AuthContext'

export default function Signup() {
  const { signUp } = useContext(AuthContext)

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const [loading, setLoading] = useState(false)

  async function handleSignUp(event: FormEvent) {
    event.preventDefault()

    if (name === '' || email === '' || password === '') {
      toast.error('Preencha todos os campos!')
      return
    }

    setLoading(true)

    const data = {
      name,
      email,
      password
    }

    await signUp({ ...data })

    setLoading(false)
  }

  return (
    <>
      <Head>
        <title>Faça seu cadastro agora!</title>
      </Head>
      <div className={styles.containerCenter}>
        <Image src={logoImg} alt="Logo Sujeito Pizzaria" />

        <div className={styles.login}>
          <h1>Criando uma conta</h1>

          <form onSubmit={handleSignUp}>
            <Input
              type={'text'}
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder={'Nome da empresa'}
            />

            <Input
              type={'email'}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder={'Digite seu email'}
            />

            <Input
              type={'password'}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder={'Digite sua senha'}
            />

            <Button loading={loading} type={'submit'}>
              Cadastrar
            </Button>
          </form>

          <Link href="/" legacyBehavior>
            <a className={styles.text}>Já possui uma conta? Faça login!</a>
          </Link>
        </div>
      </div>
    </>
  )
}
