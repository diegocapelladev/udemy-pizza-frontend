import { useState } from 'react'
import Head from 'next/head'
import Modal from 'react-modal'

import { canSSRAuth } from '../../utils/canSSRAuth'

import { Header } from '../../components/Header'

import styles from './styles.module.scss'
import { FiRefreshCcw } from 'react-icons/fi'
import { setupAPIClient } from '../../services/api'
import { ModalOrder } from '../../components/ModalOrder'

type OrderProps = {
  id: string
  table: string | number
  status: boolean
  draft: boolean
  name: string | null
}

interface HomeProps {
  orders: OrderProps[]
}

export type OrderItemProps = {
  id: string
  amount: number
  order_id: string
  product_id: string
  product: {
    id: string
    name: string
    description: string
    price: string
    banner: string
  }
  order: {
    id: string
    table: string | number
    status: boolean
    name: string | null
  }
}

export default function Dashboard({ orders }: HomeProps) {
  const [orderList, setOrderList] = useState(orders || [])

  const [modalItem, setModalItem] = useState<OrderItemProps[]>([])
  const [modalVisible, setModalVisible] = useState(false)

  function handleCloseModal() {
    setModalVisible(false)
  }

  async function handleOpenModalView(id: string) {
    const apiClient = setupAPIClient()

    const response = await apiClient.get(`/order/${id}`)

    setModalItem(response.data)
    setModalVisible(true)
  }

  async function handleFinishOrder(id: string) {
    const apiClient = setupAPIClient()

    await apiClient.put('/order', {
      order_id: id
    })

    const response = await apiClient.get('/order/list')

    setOrderList(response.data)

    setModalVisible(false)
  }

  async function handleRefreshOrders() {
    const apiClient = setupAPIClient()

    const response = await apiClient.get('/order/list')
    console.log(response.data)
    setOrderList(response.data)
  }

  Modal.setAppElement('#__next')

  return (
    <>
      <Head>
        <title>Dashboard Pizzaria</title>
      </Head>
      <div>
        <Header />

        <main className={styles.container}>
          <div className={styles.containerHeader}>
            <h1>Últimos pedidos</h1>
            <button onClick={() => handleRefreshOrders()}>
              <FiRefreshCcw size={25} color="#3fffa3" />
            </button>
          </div>

          <article className={styles.listOrders}>
            {orderList.length === 0 && (
              <span className={styles.emptyList}>
                Não há pedido aberto no momento...
              </span>
            )}

            {orderList.map((order) => (
              <section className={styles.orderItem} key={order.id}>
                <button onClick={() => handleOpenModalView(order.id)}>
                  <div className={styles.tag}></div>
                  <span>Mesa {order.table}</span>
                </button>
              </section>
            ))}
          </article>
        </main>

        {modalVisible && (
          <ModalOrder
            isOpen={modalVisible}
            onRequestClose={handleCloseModal}
            order={modalItem}
            handleFinishOrder={handleFinishOrder}
          />
        )}
      </div>
    </>
  )
}

export const getServerSideProps = canSSRAuth(async (ctx) => {
  const apiClient = setupAPIClient(ctx)

  const response = await apiClient.get('/order/list')

  return {
    props: {
      orders: response.data
    }
  }
})
