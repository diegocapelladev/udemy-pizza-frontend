import Head from 'next/head'
import { FormEvent, useState } from 'react'
import { toast } from 'react-toastify'
import { Header } from '../../components/Header'
import { Input } from '../../components/ui/Input'

import { api } from '../../services/apiClient'
import { canSSRAuth } from '../../utils/canSSRAuth'

import styles from './styles.module.scss'

export default function Category() {
  const [category, setCategory] = useState('')

  async function handleRegister(event: FormEvent) {
    event.preventDefault()

    if (category === '') {
      toast.error('Preencha o campo!')
      return
    }

    await api.post('/category', {
      name: category
    })

    toast.success('Categoria cadastrada com sucesso!')
    setCategory('')
  }

  return (
    <>
      <Head>
        <title>Nova categoria</title>
      </Head>

      <div>
        <Header />

        <main className={styles.container}>
          <h1>Cadastrar categoria</h1>

          <form
            className={styles.form}
            onSubmit={(event) => handleRegister(event)}
          >
            <Input
              type={'text'}
              value={category}
              onChange={(e) => setCategory(e.target.value)}
              placeholder={'Digite o nome da categoria'}
            />

            <button type="submit">Cadastrar</button>
          </form>
        </main>
      </div>
    </>
  )
}

export const getServerSideProps = canSSRAuth(async (ctx) => {
  return {
    props: {}
  }
})
