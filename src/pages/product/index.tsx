import { ChangeEvent, FormEvent, useState } from 'react'
import Head from 'next/head'
import { FiUpload } from 'react-icons/fi'

import { Header } from '../../components/Header'
import { Input } from '../../components/ui/Input'
import { TextArea } from '../../components/ui/Input'
import { canSSRAuth } from '../../utils/canSSRAuth'

import styles from './styles.module.scss'
import { setupAPIClient } from '../../services/api'
import { toast } from 'react-toastify'
import { api } from '../../services/apiClient'

type CategoryProps = {
  id: string
  name: string
}

interface CategoryListProps {
  categoryList: CategoryProps[]
}

export default function Product({ categoryList }: CategoryListProps) {
  const [avatarUrl, setAvatarUrl] = useState('')
  const [imageAvatar, setImageAvatar] = useState(null)

  const [categories, setCategories] = useState(categoryList || [])
  const [categorySelected, setCategorySelected] = useState(0)

  const [name, setName] = useState('')
  const [price, setPrice] = useState('')
  const [description, setDescription] = useState('')

  function handleFile(event: ChangeEvent<HTMLInputElement>) {
    if (!event.target.files) {
      return
    }

    const image = event.target.files[0]

    if (!image) {
      return
    }

    if (
      image.type === 'image/png' ||
      image.type === 'image/jpg' ||
      image.type === 'image/jpeg'
    ) {
      setImageAvatar(image)
      setAvatarUrl(URL.createObjectURL(event.target.files[0]))
    }
  }

  function handleChangeCategory(event: any) {
    setCategorySelected(event.target.value)
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault()

    try {
      const data = new FormData()

      if (
        name === '' ||
        price === '' ||
        description === '' ||
        imageAvatar === null
      ) {
        toast.error('Preencha todos os campos!')
        return
      }

      data.append('name', name)
      data.append('price', price)
      data.append('description', description)
      data.append('category_id', categories[categorySelected].id)
      data.append('file', imageAvatar)

      await api.post('/product', data)

      toast.success('Produto cadastrado!')
    } catch (err) {
      toast.error('Erro ao cadastrar!')
      console.log(err)
    }

    setName('')
    setPrice('')
    setDescription('')
    setImageAvatar(null)
    setAvatarUrl('')
  }

  return (
    <>
      <Head>
        <title>Novo produto</title>
      </Head>

      <div>
        <Header />

        <main className={styles.container}>
          <h1>Novo produto</h1>

          <form className={styles.form} onSubmit={handleSubmit}>
            <label className={styles.labelAvatar}>
              <span>
                <FiUpload size={30} color="#fff" />
              </span>

              <input
                type="file"
                accept="image/png, image/jpg, image/jpeg"
                onChange={handleFile}
              />

              {avatarUrl && (
                <img
                  className={styles.preview}
                  src={avatarUrl}
                  alt="Foto do produto"
                  width={250}
                  height={250}
                />
              )}
            </label>

            <select value={categorySelected} onChange={handleChangeCategory}>
              {categories?.map((category, index) => (
                <option key={category.id} value={index}>
                  {category.name}
                </option>
              ))}
            </select>

            <Input
              type={'text'}
              value={name}
              onChange={(e) => setName(e.target.value)}
              placeholder={'Nome do produto'}
            />
            <Input
              type={'text'}
              value={price}
              onChange={(e) => setPrice(e.target.value)}
              placeholder={'Preço do produto'}
            />

            <TextArea
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              placeholder="Descrição do produto..."
            />

            <button type="submit">Cadastrar</button>
          </form>
        </main>
      </div>
    </>
  )
}

export const getServerSideProps = canSSRAuth(async (ctx) => {
  const apiClient = setupAPIClient(ctx)

  const response = await apiClient.get('/category')

  return {
    props: {
      categoryList: response.data
    }
  }
})
