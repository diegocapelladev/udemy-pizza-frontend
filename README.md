# Pizza Project Front-end

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Pizza Project Back-end

The API can be accessed on [https://gitlab.com/diegocapelladev/udemy-pizza-frontend](https://gitlab.com/diegocapelladev/udemy-pizza-frontend)

## Pizza Project Mobile

The mobile app can be accessed on [https://gitlab.com/diegocapelladev/udemy-pizza-mobile](https://gitlab.com/diegocapelladev/udemy-pizza-mobile)

## Preview

<p align="center">
  <img alt="projeto Pizza" src=".github/pizza-preview.png" width="100%">
</p>
